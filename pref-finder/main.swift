//
//  main.swift
//  pref-finder
//
//  Created by Joel Rennich on 3/24/17.
//  Copyright © 2017 Trusource Labs. All rights reserved.
//

import Foundation

var domain: String?
var key: String?

// things to do

var printAll = false
var printKey = false
var checkForced = false
var checkForcedAll = false

// some variables

let version = ".9"

// utilty to show all keys available to an app and determine if it's forced or not

print("Starting up...")

if CommandLine.arguments.count == 1 {
    print("pref-finder")
    print("Version: " + version)
    print("A small CLI util to determine if an application has forced preference keys.")
    print("pref-finder -d <domain> [-k <key] [-fFa]")
    print("--d <domain>     sets the pref domain, e.g. com.trusourcelabs.NoMAD")
    print("-k <key>         requests the value of a key")
    print("-f               checks to see if the specfied key is forced")
    print("-F               returns all forced keys for the domain")
    print("-a               returns all keys for the domain")
    exit(0)
}

for arg in 0...(CommandLine.arguments.count - 1) {
    if CommandLine.arguments[arg] == "-d" {
        domain = CommandLine.arguments[arg+1]
    } else if CommandLine.arguments[arg] == "-k" {
        key = CommandLine.arguments[arg+1]
        printKey = true
    } else if CommandLine.arguments[arg] == "-f" {
        checkForced = true
    } else if CommandLine.arguments[arg] == "-a" {
        printAll = true
    } else if CommandLine.arguments[arg] == "-F" {
        checkForcedAll = true
    }

}

let myPrefs = Preferences(domain: domain!)

if printAll {
    print("***ALL KEYS***")
    myPrefs.printAll()
}

if printKey {
    print("Value for key: " + key!)
    myPrefs.printKey(key: key!)
}

if checkForced {
    print("Checking if forced: " + key!)
    print(myPrefs.checkForced(key: key!))
}

if checkForcedAll {
    myPrefs.checkForcedAll()
}
