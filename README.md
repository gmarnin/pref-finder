pref-finder

A simple utility to check for all keys that an Application can see and if any of them are forced.

Useage

pref-finder -d \<domain\> [-k <key] [-fFa] 

-d \<domain\>      sets the pref domain, e.g. com.trusourcelabs.NoMAD

-k \<key\>         requests the value of a key

-f               checks to see if the specfied key is forced

-F               returns all forced keys for the domain

-a               returns all keys for the domain